// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  alias: {
    //'@bojnit/forma-common': require.resolve('@bojnit/consola-common/mocks/@bojnit/forma-common'),
    '@nestjs/graphql': require.resolve('@bojnit/consola-common/mocks/@nestjs/graphql'),
    'class-validator': 'class-validator/cjs',
    'typeorm': require.resolve('@bojnit/consola-common/mocks/typeorm'),
  },

  vite: {
    server: {
      fs: {
        allow: ['../consola-common', '../../node_modules'],
      }
    }
  },

  //nitro: {
  //  moduleSideEffects: ['reflect-metadata'],
  //},

  modules: [
    '@formkit/nuxt',
    '@nuxtjs/apollo',
    //'@nuxtjs/tailwindcss',
  ],

  // @formkit/nuxt
  formkit: {
    //
  },

  // @nuxtjs/apollo
  apollo: {
    clients: {
      default: {
        httpEndpoint: 'http://localhost:12100/graphql'
      }
    },
  },

  // @nuxtjs/tailwindcss
  //tailwindcss: {
    //
  //},
});
